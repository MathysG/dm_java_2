import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
      if(liste.size()==0){
        return null;
      }
      else{
        Integer min=liste.get(0);
        for(Integer elem: liste){
          if(elem<min){
            min=elem;
          }
        }
        return min;
      }
    }


    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
      if(liste.contains(valeur)){
        return false;
      }
      else{
        List<T> listeTest = new ArrayList<>();
        for(T elem:liste){
          listeTest.add(elem);
        }
        listeTest.add(valeur);
        return Collections.min(listeTest).equals(valeur);
      }
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
      List<T> res=new ArrayList<>();
      if((liste1.size()==0)||(liste2.size()==0)||(Collections.max(liste1).compareTo(Collections.min(liste2))<0)||(Collections.max(liste2).compareTo(Collections.min(liste1))<0)){
        return res;
      }
      else{
        for(T elem:liste1){
          if(liste2.contains(elem)&&!res.contains(elem)){
            res.add(elem);
          }
        }
        Collections.sort(res);
        return res;
      }
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
      List<String> liste=new ArrayList<>();
      if(texte.length()==0){
        return liste;
      }
      else{
        int depart=0;
        int procEspace=texte.indexOf(' ',depart);
        while(procEspace>=0){
          if(procEspace-depart>1){
            liste.add(texte.substring(depart,procEspace));
          }
          depart=procEspace+1;
          procEspace=texte.indexOf(' ',depart);
        }
        String dernierMot=texte.substring(depart);
        if(dernierMot.length()>0){
          liste.add(dernierMot);
        }
        return liste;
      }
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
      if (texte.length()==0){
        return null;
      }
      List<String> liste=new ArrayList<>(BibDM.decoupe(texte));
      HashMap<String,Integer> dicoFreq = new HashMap<>();
      for(String elem:liste){
        if(dicoFreq.containsKey(elem)){
          dicoFreq.put(elem,dicoFreq.get(elem)+1);
        }
        else{
          dicoFreq.put(elem,1);
        }
      }
      List<String> listeMax=new ArrayList<>();
      HashSet<Integer> ensembleValeur= new HashSet<>(dicoFreq.values());
      Integer valMax=Collections.max(ensembleValeur);
      for(String clé:dicoFreq.keySet()){
        if(dicoFreq.get(clé)==valMax){
          listeMax.add(clé);
        }
      }
      return Collections.min(listeMax);
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parenthèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
      int nbOuvrante=0;
      int nbFermante=0;
      int index=0;
      while(index<chaine.length()){
        char carac=chaine.charAt(index);
        if(carac=='('){
          nbOuvrante+=1;
        }
        else if(carac==')'){
          nbFermante+=1;
          if(nbFermante>nbOuvrante){
            return false;
          }
        }
        index+=1;
      }
      return nbOuvrante==nbFermante;
    }


    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parenthèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
     int nbCroOuvrant=0;
     int nbCroFermant=0;
     List<Integer> indexCroOuvrant=new ArrayList<>();
     int index=0;
     if(!BibDM.bienParenthesee(chaine)){return false;}
     while(index<chaine.length()){
       char carac=chaine.charAt(index);
       if(carac=='['){
         // nbCroOuvrant+=1;
         indexCroOuvrant.add(index);
       }
       else if(carac==']'){
         // nbCroFermant+=1;
         if(indexCroOuvrant.size()==0){return false;}
         String sousChaine=chaine.substring(indexCroOuvrant.get(indexCroOuvrant.size()-1),index);
         Integer elemSup=indexCroOuvrant.remove(indexCroOuvrant.size()-1);
         if(!BibDM.bienParenthesee(sousChaine)){return false;}
       }
       index+=1;
     }
     return indexCroOuvrant.size()==0;
   }


    /**
     * Recherche par dichotomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
      if(liste.size()==0){
        return false;
      }
      else{
        int indexDep=0;
        int indexFin=liste.size()-1;
        while(indexFin-indexDep>1){
          if(valeur==liste.get(indexDep)||valeur==liste.get(indexFin)){return true;}
          else if(valeur>liste.get(indexDep)&&valeur<liste.get(indexFin)){
            if(valeur<liste.get((indexFin+indexDep)/2)){
              indexFin=((indexFin+indexDep)/2)+1;
            }
            else{
              indexDep=((indexFin+indexDep)/2);
            }
          }
          else{
            return false;
          }
        }
        return (valeur==liste.get(indexDep)||valeur==liste.get(indexFin));
      }
    }
  }
